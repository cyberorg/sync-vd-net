package syncSrv;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import java.io.IOException;
 
import org.eclipse.jetty.server.Server;

import org.eclipse.jetty.servlet.*;


public class SyncServer {
	public static void main(String[] args) throws Exception
    {
        Server server = new Server(9501);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
 
        context.addServlet(new ServletHolder(new HelloServlet()),"/*");
        server.start();
        server.join();
    }
  static public class HelloServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;
    public HelloServlet(){}
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {	
    	//in
    	try{
    	String in = request.getParameter("data");
    	if(in.equals("")){
    		response.setContentType("text/html");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println();
    	}
    	//app (Stub)
    	String result = "<h1>It Works!</h1>";   	
    	//out
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(result);
    	}catch(NullPointerException e){
    		 response.setContentType("text/html");
    	     response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    	     response.getWriter().println();
    	}catch (Exception e) {
    		 e.printStackTrace();
    		 response.setContentType("text/html");
    	     response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
    	     response.getWriter().println();
		}
    }
}

} 

